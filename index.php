<?php
require_once('animal.php');
require_once('frog.php');
require_once('ape.php');


$sheep = new Animal("shaun");
echo $sheep->get_names() ;
echo "<br>"; // "shaun"
echo $sheep->get_legs() ;
echo "<br>"; // 2
echo json_encode($sheep->get_cold_blooded()); // false
echo "<br>";
echo "<br>";
$kodok = new Frog("buduk");
echo $kodok->get_names() ;
echo "<br>";
echo $kodok->get_legs() ;
echo "<br>";
echo json_encode($kodok->get_cold_blooded());
echo "<br>";
$kodok->jump() ;// "hop hop"
echo "<br>";
echo "<br>";
$sungokong = new Ape("Kera Sakti");
echo $sungokong->get_names() ;
echo "<br>";
echo $sungokong->get_legs() ;
echo "<br>";
echo json_encode($kodok->get_cold_blooded());
echo "<br>";
$sungokong->yell();
?>
